// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GB : ModuleRules
{
	public GB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput","HeadMountedDisplay", "NavigationSystem", "AIModule" , "PhysicsCore", "Slate" });
	}
}
