// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GBGameMode.generated.h"

UCLASS(minimalapi)
class AGBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGBGameMode();
};



