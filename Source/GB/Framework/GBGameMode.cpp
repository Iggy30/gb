// Copyright Epic Games, Inc. All Rights Reserved.

#include "GBGameMode.h"
#include "GB/Character//GBCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGBGameMode::AGBGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
